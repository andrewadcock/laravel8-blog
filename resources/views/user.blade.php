<x-layout>
    @section('content')
        <div class="posts">
            <h1><a href="/">Welcome to the Blog!</a></h1>
            @if ($user)
{{--                {{ dd($user) }}--}}
                <div class="user-info">
                    <div>Name: {{ $user->name }}</div>
                    <div>Email: {{ $user->email }}</div>
                </div>

            @endif
            @foreach ($posts as $post)
{{--                {{{ ddd($post) }}}--}}
                <article class="{{ $loop->even ? "bg-dark" : '' }}">
                    <h1>
                        <a href="/posts/{{ $post->slug }}">
                            {!! $post->title !!}
                        </a>
                    </h1>
{{--                    <p>--}}
{{--                        Written by <a href="/authors/">{{ $post->user->name }}</a> in category <a href="/categories/{{ $post->category->slug }}">{{ $post->category->name }}</a>--}}
{{--                    </p>--}}
                    <div class="meta">Published: {{ $post->date }}</div>
                    <div class="excerpt">{{ $post->excerpt }}</div>
                </article>
            @endforeach
        </div>
    @endsection
</x-layout>
