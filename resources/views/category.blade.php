<x-layout>
    @section('content')
        <div class="posts">
            <h1>Category: {{ $title }}</h1>
            <a href="/categories">Back to Categories</a>
{{--            {{ dd($posts) }}--}}
{{--            {{ dd($title) }}--}}
            @foreach ($posts as $post)
                <article class="{{ $loop->even ? "bg-dark" : '' }}">
                    <h1>
                        <a href="/posts/{{ $post->slug }}">
                            {!! $post->title !!}
                        </a>
                    </h1>
                    <p>
                        Category: <a href="/categories/{{ $post->category->slug }}">{{ $post->category->name }}</a><br />
                        Author: <a href="/authors/{{ $post->author->username }}">{{ $post->author->name }}</a>
                    </p>
                    <div class="meta">Published: {{ $post->date }}</div>
                    <div class="excerpt">{{ $post->excerpt }}</div>
                </article>
            @endforeach
        </div>
    @endsection
</x-layout>
