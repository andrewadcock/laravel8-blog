<x-layout>
    @section('content')
        <article>
            <h1>{!! $post->title !!}</h1>
            <p>
                Written by <a href="/authors/{{ $post->author->username }}">{{$post->author->name}}</a> in category <a href="/categories/{{ $post->category->slug }}">{{ $post->category->name }}</a>
            </p>
            <div class="meta">Published: {{ $post->date }}</div>
            <div>{!! $post->body !!}</div>
        </article>

        <a href="/">Go Back</a>
    @endsection
</x-layout>
