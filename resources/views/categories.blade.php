<x-layout>
    @section('content')
        <div class="posts">
            <h1>Categories</h1>
{{--            {{ dd($categories) }}--}}
            @foreach ($categories as $cat)
                <article class="{{ $loop->even ? "bg-dark" : '' }}">
                    <h1>
                        <a href="/categories/{{ $cat->slug }}">
                            {!! $cat->name !!}
                        </a>
                    </h1>

                </article>
            @endforeach
        </div>
    @endsection
</x-layout>
