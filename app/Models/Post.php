<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Post extends Model
{
    use HasFactory;

    // What you can NOT mass fill in
//    protected $guarded = [];

    // What you CAN mass fill in
//    protected $fillable = ['title', 'excerpt', 'body'];

//    protected $with = ['category', 'author']; // Forces all queries to return category and author; could be extraneous

    /**
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function category(): BelongsTo
    {
        //hasOne, hasMany, belongsTo, belongsToMany
        return $this->belongsTo(Category::class);
    }

    public function author(): object
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
