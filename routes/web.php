<?php
use App\Models\Post;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Spatie\YamlFrontMatter\YamlFrontMatter;
use Illuminate\Support\Facades\File;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

//    \Illuminate\Support\Facades\DB::listen(function ($query) {
//        logger($query->sql);
//    });

    return view('posts', [
        'posts' => Post::latest()->with(['category', 'author'])->get(),
    ]);
})->name('home');

Route::get('/posts/{post}', function (Post $post) {
    return view('post', [
        'post' => $post,
    ]);

})->where('post', '[A-z_\-]+');

Route::get('categories/{category:slug}', function (Category $category) {

   return view('category', [
       'posts' => $category->posts->load(['category', 'author']),
       'title' => Category::query()->select('name')->where('slug', '=', $category->slug)->get()[0]->name
   ]);
});

Route::get('categories', function () {

    return view('categories', [
        'categories' => Category::all()
    ]);
});

Route::get('authors/{author:username}', function (User $author) {

    return view('user', [
        'posts' => $author->posts->load(['category', 'author']),
        'user' => $author,
    ]);
});
