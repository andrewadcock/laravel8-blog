<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        User::truncate();
//        Category::truncate();
//        Post::truncate();

        // Push in some specific data into the factory
        $users = User::factory(5)->create([
//            'name' => 'John Doe',
        ]);

        $i = 0;
        while($i < 5) {
            Post::factory(1)->create([
                'user_id' => User::all()->random()->id
            ]);
            $i++;
        }

//        $user = User::factory()->create();


//        $personal = Category::create([
//            'name' => 'Personal',
//            'slug' => 'personal'
//        ]);
//
//        $family = Category::create([
//            'name' => 'Family',
//            'slug' => 'family'
//        ]);
//
//        $work = Category::create([
//            'name' => 'Work',
//            'slug' => 'work'
//        ]);
//
//        Post::create([
//            'user_id' => $user->id,
//            'category_id' => $family->id,
//            'title' => 'My First Post',
//            'slug' => 'my-first-post',
//            'excerpt' => 'This is the first post excerpt',
//            'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo justo vel sapien porta luctus. Nunc nec nisl suscipit, mattis velit eget, tristique arcu. Duis velit arcu, iaculis in sagittis sed, rhoncus quis lorem. Quisque vel viverra turpis. Sed ut volutpat arcu. Suspendisse scelerisque vulputate enim non dapibus. Nam feugiat vel ligula eu dignissim. Sed sapien purus, semper tincidunt quam sit amet, rhoncus tempor libero. Curabitur sed volutpat sem, eu placerat est. Praesent eget dui sit amet lacus sollicitudin vehicula. Suspendisse facilisis nisi ut ligula blandit, convallis semper turpis pharetra. Sed eget massa ut diam bibendum posuere ac eget velit.</p><p>Suspendisse fringilla accumsan vestibulum. In hac habitasse platea dictumst. Donec id justo dui. Praesent ex sem, ultrices eget ante id, pharetra aliquam ipsum. Sed vitae tincidunt mi. Nunc eget libero urna. Donec orci dolor, consequat nec augue quis, scelerisque laoreet sapien. Pellentesque fringilla placerat elit quis consectetur. Sed at dui nec quam commodo ullamcorper.</p>',
//        ]);
//
//        Post::create([
//            'user_id' => $user->id,
//            'category_id' => $personal->id,
//            'title' => 'My Second Post',
//            'slug' => 'my-second-post',
//            'excerpt' => 'This is the first post excerpt',
//            'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo justo vel sapien porta luctus. Nunc nec nisl suscipit, mattis velit eget, tristique arcu. Duis velit arcu, iaculis in sagittis sed, rhoncus quis lorem. Quisque vel viverra turpis. Sed ut volutpat arcu. Suspendisse scelerisque vulputate enim non dapibus. Nam feugiat vel ligula eu dignissim. Sed sapien purus, semper tincidunt quam sit amet, rhoncus tempor libero. Curabitur sed volutpat sem, eu placerat est. Praesent eget dui sit amet lacus sollicitudin vehicula. Suspendisse facilisis nisi ut ligula blandit, convallis semper turpis pharetra. Sed eget massa ut diam bibendum posuere ac eget velit.</p><p>Suspendisse fringilla accumsan vestibulum. In hac habitasse platea dictumst. Donec id justo dui. Praesent ex sem, ultrices eget ante id, pharetra aliquam ipsum. Sed vitae tincidunt mi. Nunc eget libero urna. Donec orci dolor, consequat nec augue quis, scelerisque laoreet sapien. Pellentesque fringilla placerat elit quis consectetur. Sed at dui nec quam commodo ullamcorper.</p>',
//        ]);
//
//        Post::create([
//            'user_id' => $user->id,
//            'category_id' => $work->id,
//            'title' => 'My Third Post',
//            'slug' => 'my-third-post',
//            'excerpt' => 'This is the first post excerpt',
//            'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo justo vel sapien porta luctus. Nunc nec nisl suscipit, mattis velit eget, tristique arcu. Duis velit arcu, iaculis in sagittis sed, rhoncus quis lorem. Quisque vel viverra turpis. Sed ut volutpat arcu. Suspendisse scelerisque vulputate enim non dapibus. Nam feugiat vel ligula eu dignissim. Sed sapien purus, semper tincidunt quam sit amet, rhoncus tempor libero. Curabitur sed volutpat sem, eu placerat est. Praesent eget dui sit amet lacus sollicitudin vehicula. Suspendisse facilisis nisi ut ligula blandit, convallis semper turpis pharetra. Sed eget massa ut diam bibendum posuere ac eget velit.</p><p>Suspendisse fringilla accumsan vestibulum. In hac habitasse platea dictumst. Donec id justo dui. Praesent ex sem, ultrices eget ante id, pharetra aliquam ipsum. Sed vitae tincidunt mi. Nunc eget libero urna. Donec orci dolor, consequat nec augue quis, scelerisque laoreet sapien. Pellentesque fringilla placerat elit quis consectetur. Sed at dui nec quam commodo ullamcorper.</p>',
//        ]);
//
//        Post::create([
//            'user_id' => $user->id,
//            'category_id' => $family->id,
//            'title' => 'My Fourth Post',
//            'slug' => 'my-fourth-post',
//            'excerpt' => 'This is the first post excerpt',
//            'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo justo vel sapien porta luctus. Nunc nec nisl suscipit, mattis velit eget, tristique arcu. Duis velit arcu, iaculis in sagittis sed, rhoncus quis lorem. Quisque vel viverra turpis. Sed ut volutpat arcu. Suspendisse scelerisque vulputate enim non dapibus. Nam feugiat vel ligula eu dignissim. Sed sapien purus, semper tincidunt quam sit amet, rhoncus tempor libero. Curabitur sed volutpat sem, eu placerat est. Praesent eget dui sit amet lacus sollicitudin vehicula. Suspendisse facilisis nisi ut ligula blandit, convallis semper turpis pharetra. Sed eget massa ut diam bibendum posuere ac eget velit.</p><p>Suspendisse fringilla accumsan vestibulum. In hac habitasse platea dictumst. Donec id justo dui. Praesent ex sem, ultrices eget ante id, pharetra aliquam ipsum. Sed vitae tincidunt mi. Nunc eget libero urna. Donec orci dolor, consequat nec augue quis, scelerisque laoreet sapien. Pellentesque fringilla placerat elit quis consectetur. Sed at dui nec quam commodo ullamcorper.</p>',
//        ]);
    }
}
